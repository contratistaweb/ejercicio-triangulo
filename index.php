<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Area de triangulo circunscripto</title>
</head>

<body>
    <div class="container">
        <div class="row jumbotron p-5">
            <form action="index.php" method="post" class="col-4 card shadow ml-auto mt-auto">
                <div class="form-group form-column my-3">
                    <label for="radio" class="col text-center">Ingrese el valor del radio:</label>
                    <input type="text" name="radio" id="radio" class="form-control col">
                </div>
                <div class="d-flex flex-row justify-content-center my-3">
                    <button type="submit" class="btn btn-success col-6">Calcular</button>
                </div>
            </form>
            <hr>
            <div class="col-12">
                <?php
                // $radio = 0;
                error_reporting(0);

                if ($_REQUEST['radio'] != '') {
                    $radio = $_REQUEST['radio'];
                    $calArea = 3 * pow($radio, 2) * sqrt(3);
                    $calPer = 6 * $radio * sqrt(3);
                    echo '<hr><p>Radio: ' . $radio . '</p>';
                    echo '<p>Area: ' . $calArea . '</p>';
                    echo '<p>perimetro: ' . $calPer . '</p><hr>';
                } else {
                    echo '<hr><p>En esta area se imprimira el resultado.</p><hr>';
                }
                ?>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
</body>

</html>